package learn.c05;

/**
 * 作者: crazier.huang
 * 项目: zookeeper
 * 日期: 2018/12/2 Sunday
 * 说明:
 */
public final class Constant {

    /**
     * zk地址
     */
    public final static  String ZK_ADDR = "39.105.49.44:2181";
}
