package learn.c05;


import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * 作者: crazier.huang
 * 项目: zookeeper_book
 * 日期: 2018/11/27 Tuesday
 * 说明:
 */
public class ZooKeeper_Constructor_Usage_Simple implements Watcher {

    private static CountDownLatch connectedSemaphore = new CountDownLatch(1);


    public static void main(String[] args) throws IOException {
        ZooKeeper zooKeeper = new ZooKeeper("39.105.49.44:2181",5000,new ZooKeeper_Constructor_Usage_Simple());
        System.out.println(zooKeeper.getState());
        try {
            connectedSemaphore.await();
            System.out.println("唤醒当前线程");
        } catch (InterruptedException e) {
            System.out.println("ZooKeeper session established.");
        }
    }
    @Override
    public void process(WatchedEvent event) {
        System.out.println("Receive watched event：" + event);
        if(Event.KeeperState.SyncConnected == event.getState()){
            connectedSemaphore.countDown();
        }
    }
}
