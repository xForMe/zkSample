package learn.c05;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * 作者: crazier.huang
 * 项目: zookeeper
 * 日期: 2018/11/27 Tuesday
 * 说明:
 */
public class ZooKeeper_Constructor_Usage_With_SID_PASSWD implements Watcher {
    private static CountDownLatch connectedSemaphore = new CountDownLatch(1);

    public static void main(String[] args) throws IOException, InterruptedException {
        ZooKeeper zooKeeper = new ZooKeeper("39.105.49.44:2181",5000,new ZooKeeper_Constructor_Usage_With_SID_PASSWD());
        long sessionId = zooKeeper.getSessionId();
        byte[] passwd  = zooKeeper.getSessionPasswd();
        zooKeeper = new ZooKeeper("39.105.49.44:2181",5000,
                new ZooKeeper_Constructor_Usage_With_SID_PASSWD(),
                1l,"test".getBytes()
        );
        zooKeeper= new ZooKeeper("39.105.49.44:2181",5000,
                new ZooKeeper_Constructor_Usage_With_SID_PASSWD(),
                sessionId,passwd
        );
        Thread.sleep(Integer.MAX_VALUE);

    }

    @Override
    public void process(WatchedEvent event) {
        System.out.println("Receive watched event：" + event);
        if(Event.KeeperState.SyncConnected == event.getState()){
            connectedSemaphore.countDown();
        }
    }
}
