package learn.c05.learnc0541;

import learn.c05.Constant;
import org.I0Itec.zkclient.ZkClient;

/**
 * 作者: crazier.huang
 * 项目: zookeeper
 * 日期: 2018/12/1 Saturday
 * 说明:
 */
public class Exist_Node_Sample {
//
//    ZkClient zkClient = new ZkClient("domain1.book.zookeeper:2181", 5000);
//    String path = "/zk-book/c1";
//        zkClient.createPersistent(path, true);

    public static void main(String[] args){
        ZkClient zkClient = new ZkClient(Constant.ZK_ADDR,5000);
        String path = "/zk-book";
        //创建持久化节点
        Boolean exist = zkClient.exists(path);
        System.out.println("Node " + path + " exists " + zkClient.exists(path));
    }
}
