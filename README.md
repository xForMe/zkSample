# zookeeper  入门教程

## 节点类型
- 持久节点
- 持久顺序节点
- 临时节点
- 临时顺序节点

**创建持久化节点**


```
    public static void main(String[] args){
        ZkClient zkClient = new ZkClient(Constant.ZK_ADDR,5000);
        String path = "/zk-book/c1";
        //创建持久化节点
        zkClient.createPersistent(path,true);
    }
```

**判断节点是否存在**


```
    public static void main(String[] args){
        ZkClient zkClient = new ZkClient(Constant.ZK_ADDR,5000);
        String path = "/zk-book";
        //创建持久化节点
        Boolean exist = zkClient.exists(path);
        System.out.println("Node " + path + " exists " + zkClient.exists(path));
    }
```
